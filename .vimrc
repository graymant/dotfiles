"======= Mappings ========

let mapleader = ","
inoremap jj <Esc>

nmap <leader>lc :lclose<CR>
nmap <leader>noh :noh<CR>
nmap <C-1> :w<cr>
nnoremap <leader>qq :q<CR>
nnoremap <leader>qa :qa<CR>
nnoremap <leader>ww :w<CR>
nnoremap <leader>ee :Bclose<CR>
nnoremap <leader>ra :bufdo e!<CR>
nnoremap <leader>fff gg=G<CR>

noremap <leader>mm :make<CR>
noremap <leader>co :Copen<CR>
noremap <leader>sm :messages<CR>

nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

nnoremap <leader>grr :!git co %<cr>
nnoremap <leader>ccl :ccl<cr>
nnoremap <leader>lia :cl<cr>

nmap <silent> <leader>cd :lcd %:h<CR>
nmap <silent> <leader>ul :t.<CR>Vr=
nmap <silent> <leader>tw :set invwrap<cr>:set wrap?<cr>

nnoremap <leader>su :execute 'sign unplace * buffer=' . bufnr('')<CR>
nnoremap <leader><leader> <C-^>

vmap <C-k> [egv
vmap <C-j> ]egv

" System clipboard
vmap <space>y "+y
nmap <space>y "+yy
nmap <Leader>yy "+yy
nmap <Leader>pp "+p
nmap <space>p "+p

set pastetoggle=<Leader>pt

" ======= General Vim Settings ============"

set shiftwidth=4
set tabstop=4
set backspace=indent,eol,start
set hidden

set nowrap
set incsearch
set hlsearch
set ignorecase
set smartcase
set gdefault
set number
set showmatch
set autoread
set smarttab
set complete-=i
:set ruler
set laststatus=2
set wildmenu
set ttimeout
set ttimeoutlen=100

if v:version > 703 || v:version == 703 && has("patch541")
	set formatoptions+=j " Delete comment character when joining commented lines
endif

if has('path_extra')
	setglobal tags-=./tags tags-=./tags; tags^=./tags;
endif

" Allow color schemes to do bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^Eterm'
	set t_Co=16
endif

if has('autocmd')
	filetype plugin indent on
endif

" set autochdir
set autowriteall
set autowrite

colorscheme desert

set nocompatible              " be iMproved, required
filetype off                  " required

set dir-=.
" set backupdir-=.
" set directory-=.

set backupdir^=~/.vim/_backup//    " where to put backup files.
set directory^=~/.vim/_temp//      " where to put swap files

set list                      " Show invisible characters
func! ListCharsCommon()
	set listchars+=trail:.            " show trailing spaces as dots
	set listchars+=extends:>          " The character to show in the last column when wrap is
	" off and the line continues beyond the right of the screen
	set listchars+=precedes:<         " The character to show in the last column when wrap is
	" off and the line continues beyond the left of the scree
	"
endfunc

func! ListCharsShowTabs()
	" List chars
	set listchars=""                  " Reset the listchars
	" set listchars=tab:\ \             " a tab should display as "  ", trailing whitespace as "."
	set listchars=tab:-.             " a tab should display as "  ", trailing whitespace as "."

	call ListCharsCommon()
endfunc
func! ListCharsHideTabs()
	" List chars
	set listchars=""                  " Reset the listchars
	set listchars=tab:\ \             " a tab should display as "  ", trailing whitespace as "."

	call ListCharsCommon()
endfunc
command! ShowTabs :call ListCharsShowTabs()
command! HideTabs :call ListCharsHideTabs()

HideTabs

"set statusline^=%{coc#status()}

"""""""""""" Vimdiff merge settings """""""""""""

noremap <leader>dgl :diffget LO<CR>
noremap <leader>dgb :diffget BA<CR>
noremap <leader>dgr :diffget RE<CR>

if &diff
	" diff mode only ignore whitespace in comparisons
	set diffopt+=iwhite
endif

"""""""""""" Grep settings """""""""""""

set grepprg=ag\ --nogroup\ --column\ $*
set grepformat=%f:%l:%c:%m

"""""""""""" Ale settings """""""""""""

let g:ale_completion_enabled = 1

"""""""""""" FZF settings """""""""""""

nnoremap <leader>fzf :FZF<CR>
nnoremap <leader>buf :Buffers<CR>

"""""""""""" Language Specific Settings """"""""""""""

"""""""""""" Python settings """""""""""""

augroup pythonag
	autocmd!
	func! FormatPython()
		let l:winview = winsaveview()
		:exe '%!black -q -'
		call winrestview(l:winview)
	endfunc
	au FileType python nmap <leader>gto <Plug>(coc-definition)
	au FileType python nmap <leader>so :call CocActionAsync('runCommand', 'python.sortImports')<CR>
	au FileType python nmap <leader>rn <Plug>(coc-rename)
	au FileType python nnoremap <leader>fff :call FormatPython()<CR>
augroup END

"""""""""""" Javascript settings """""""""""""

augroup javascriptag
	autocmd!
	func! FormatJs()
		let l:winview = winsaveview()
		:exe '%!yarn --silent prettier --parser babel --trailing-comma es5'
		call winrestview(l:winview)
	endfunc

	func! FormatJsNoSemis()
		let l:winview = winsaveview()
		:exe '%!npx -q prettier --parser babel --trailing-comma es5 --no-semi'
		call winrestview(l:winview)
	endfunc

	func! FormatJsSnippetNoSemis()
		silent exe "'<,'>!npx -q prettier --parser babel --trailing-comma es5 --no-semi"
	endfunc

	:autocmd FileType javascript :iabbrev <buffer> iff if ()<left>
	let g:jsx_ext_required = 0

	autocmd FileType javascript set shiftwidth=2 tabstop=2 expandtab
	au FileType javascript nnoremap <leader>jsr :silent exe '%s/;//'<CR>
	au FileType javascript nnoremap <leader>fff :call FormatJs()<CR>
	au FileType javascript.jsx nnoremap <leader>fff :call FormatJs()<CR>
	au FileType javascript nnoremap <leader>ffn :call FormatJsNoSemis()<CR>
	au FileType javascript vnoremap <leader>fff :!yarn prettier --trailing-comma es5<CR>
	au FileType javascript vnoremap <leader>ffn :call FormatJsSnippetNoSemis()<CR>
augroup END

""""""""""" Json formatting """""""""""""""

augroup jsontag
	autocmd!

	func! FormatJson()
		let l:winview = winsaveview()
		:exe '%!jq ''.'''
		call winrestview(l:winview)
	endfunc

	au FileType json nnoremap <leader>fff :call FormatJson()<CR>
augroup END

""""""""""" Zsh settings """"""""""""""""""""""

augroup zshag
	autocmd!
	autocmd FileType zsh set shiftwidth=4 tabstop=4
augroup END

""""""""""" Vimfile settings """"""""""""""""""""""

augroup vimag
	autocmd!
	autocmd FileType vim set shiftwidth=4 tabstop=4 noexpandtab
	au FileType vim nnoremap <leader>fff gg=G<CR>
augroup END


""""""""""" Arduino settings """""""""""""""

au BufRead,BufNewFile *.pde set filetype=cpp
au BufRead,BufNewFile *.ino set filetype=cpp

augroup arduinoac
	autocmd!
	autocmd FileType arduino setlocal shiftwidth=4 tabstop=4 cindent
	autocmd FileType arduino setlocal makeprg=cd\ ..\ &&\ ino\ build
	autocmd FileType arduino nnoremap <leader>iu :exe '!cd .. && ino upload'<CR>
augroup END

""""""""""" Rust settings """""""""""""""

func! FormatRust()
	let l:winview = winsaveview()
	:exe '%!rustfmt -- --emit stdout'
	call winrestview(l:winview)
endfunc

augroup rustaf
	autocmd!
	"au FileType rust nnoremap <leader>fff :call FormatRust()<CR>
	au FileType rust nnoremap <leader>fff :RustFmt<CR>
	autocmd FileType rust nnoremap <leader>rrt :RustTest<CR>
augroup END

let g:rustfmt_autosave = 1

""""""""""" GLSL settings """""""""""""""

func! FormatGLSL()
	let l:winview = winsaveview()
	:exe '%!clang-format'
	call winrestview(l:winview)
endfunc

augroup glslaf
	autocmd!
	autocmd FileType glsl set shiftwidth=2 tabstop=2 expandtab
	au FileType glsl nnoremap <leader>fff :call FormatGLSL()<CR>
	autocmd FileType glsl set omnifunc=set omnifunc=ale#completion#OmniFunc
augroup END


""""""""""" Markdown settings """""""""""""""

augroup markdownac
	autocmd!
	autocmd FileType mkd setlocal spell
augroup END

func! FormatMD()
	let l:winview = winsaveview()
	:exe '%!par w120'
	call winrestview(l:winview)
endfunc

nnoremap <leader>fmd :call FormatMD()<CR>
vnoremap <leader>pft !par w120<CR>

""""""""""" Coc Vim """"""""""""

set updatetime=300
set shortmess+=c

function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <TAB>
			\ pumvisible() ? "\<C-n>" :
			\ <SID>check_back_space() ? "\<TAB>" :
			\ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current " position. Coc only does snippet and additional edit on confirm.
"if exists('*complete_info')
"	inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
"else
"	imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"endif

function! s:show_documentation()
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	else
		call CocAction('doHover')
	endif
endfunction

nnoremap <silent> K :call <SID>show_documentation()<CR>
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

autocmd CursorHold * silent call CocActionAsync('highlight')
nmap <leader>rn <Plug>(coc-rename)

xmap <leader>f  <Plug>(coc-format-selected)

"autocmd InsertEnter unite execute "silent! CocDisable"
" autocmd InsertEnter unite execute "silent! CocDisable"
" autocmd InsertLeave unite execute "silent! CocEnable"

"au BufEnter unite :CocDisable
"au BufLeave unite :CocEnable

autocmd BufEnter * if &filetype == "unite"
                   \ | execute "silent! CocDisable"
                   \ | endif
autocmd BufLeave * if &filetype == "unite"
                   \ | execute "silent! CocEnable"
                   \ | endif

""""""""""" Unite """""""""""""""
let g:unite_source_history_yank_enable = 1
let g:unite_source_file_rec_max_cache_files = 0
"let g:unite_source_rec_async_command=['pt', '/l',
"			\ '""', '.', '.']
let g:unite_source_rec_async_command=['fd', ""]

nnoremap <space>u :silent up \| :Unite -start-insert file_rec/async<cr>
nnoremap <space>i :Unite -start-insert file_rec/git<cr>
nnoremap <leader>uunc :Unite -quick-match change<cr>
nnoremap <space>c :Unite -quick-match change<cr>
nnoremap <leader>u/ :silent up \| :Unite grep:.<cr>
nnoremap <silent> <leader>ug :<C-u>Unite grep:. -buffer-name=search-buffer<CR>
nnoremap <leader>ur :UniteResume<cr>
nnoremap <leader>un :UniteNext<cr>
nnoremap <leader>un :UnitePrevious<cr>
nnoremap <leader>bb :Unite -quick-match buffer<cr>
nnoremap <leader>uf :Unite function<cr>
" nnoremap <space>y :Unite -quick-match history/yank<cr>
nnoremap <space>f :silent up \| :Unite -start-insert buffer<cr>

nnoremap <leader>gg :execute 'Unite gtags/def'<CR>
nnoremap <leader>gc :execute 'Unite gtags/context'<CR>
nnoremap <leader>gr :execute 'Unite gtags/ref'<CR>
nnoremap <leader>ge :execute 'Unite gtags/grep'<CR>
vnoremap <leader>gg <ESC>:execute 'Unite gtags/def:'.GetVisualSelection()<CR>
nnoremap <space>* :execute "Unite grep:.::<C-R><C-W>:"<CR>

nnoremap <leader>uo :Unite outline<CR>
nnoremap <leader>ut :Unite -start-insert tag<CR>

" Can filter if required ie :Unite -input=foo file

nnoremap <leader>of :<C-u>Unite -start-insert file/async<CR>

if executable('pt')
	let g:unite_source_grep_command = 'pt'
	let g:unite_source_grep_default_opts = '-i --nogroup --nocolor'
	let g:unite_source_grep_recursive_opt = ''
	let g:unite_source_grep_encoding = 'utf-8'
endif

""""""""""" Easy motion """""""""""""""

let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Bi-directional find motion
" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
" nmap s <Plug>(easymotion-s)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
nmap <space>e <Plug>(easymotion-s2)

" Turn on case sensitive feature
let g:EasyMotion_smartcase = 1

map  <space>/ <Plug>(easymotion-sn)
omap <space>/ <Plug>(easymotion-tn)
nmap <space>w <Plug>(easymotion-w)
nmap <space>b <Plug>(easymotion-b)

" JK motions: Line motions
map <space>j <Plug>(easymotion-j)
map <space>k <Plug>(easymotion-k)
map <space>l <Plug>(easymotion-lineforward)
map <space>h <Plug>(easymotion-linebackward)

""""""""""" Vim slime motion """""""""""""""

let g:slime_target = "vimterminal"
" let g:slime_vimterminal_cmd = 'python'

let g:slime_no_mappings = 1
xmap <leader>sls <Plug>SlimeRegionSend
nmap <leader>sls <Plug>SlimeParagraphSend
nmap <leader>slc     <Plug>SlimeConfig

""""""""""" Misc motion """""""""""""""

let g:polyglot_disabled = ['latex']

""""""""""" Plugins """""""""""""""

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-surround'
Plug 'tpope/vim-rsi'
Plug 'tpope/vim-endwise'
Plug 'https://github.com/Lokaltog/vim-easymotion.git'
Plug 'https://github.com/Shougo/vimproc.vim.git', { 'do': 'make' }
Plug 'https://github.com/Shougo/unite.vim.git'
Plug 'Shougo/unite-outline'
Plug 'tsukkee/unite-tag'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'w0rp/ale'
Plug 'sheerun/vim-polyglot'
Plug 'https://github.com/vadimr/bclose.vim.git'
Plug 'https://github.com/flazz/vim-colorschemes.git'
" Plug 'lervag/vimtex'
Plug 'vim-airline/vim-airline'
Plug 'bronson/vim-trailing-whitespace'
Plug 'ddollar/nerdcommenter'
Plug 'luochen1990/rainbow'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'jpalardy/vim-slime'
Plug 'kassio/neoterm'


call plug#end()
